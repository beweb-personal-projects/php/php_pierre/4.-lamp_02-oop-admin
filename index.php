<?php 
session_start();
require('classes/classes.php');
require('data.php');

if(!isset($_SESSION['all_players'])) $_SESSION['all_players'] = [];
if(!isset($_SESSION['all_staffs'])) $_SESSION['all_staffs'] = [];

function sortAllPlayers() {
    global $data;

    $_SESSION['all_players'] = [];
    $_SESSION['all_staffs'] = [];

    foreach ($data as $i => $player) {
        setAllPlayers($player['name'], $player['job'], $player['job_rank']);

        if($player['role'] !== 'user' || $player['ignorePermissions']) {
            setAllStaffs($player['name'], $player['role'], $player['level'], $player['ignorePermissions']);
        }
    }
}

function setAllPlayers($name, $job, $job_rank) {
    $temp_player = new Player($name, $job, $job_rank);
    array_push($_SESSION['all_players'], $temp_player);
}

function setAllStaffs($name, $role, $level, $ignorePermissions) {
    $temp_staff = new Staff($name, $role, $level, $ignorePermissions);
    array_push($_SESSION['all_staffs'], $temp_staff);
}

sortAllPlayers();

function changePlayerName($currentPlayerName, $newName) {
    foreach ($_SESSION['all_players'] as $i => $player) {
        if($player->getName() === $currentPlayerName) {
            $player->setName($newName);

            foreach ($_SESSION['all_staffs'] as $j => $staff) {
                if($staff->getName() === $currentPlayerName) {
                    $staff->setName($newName);
                }
            }

            return;
        }
    }
}

changePlayerName("strackz2", "strackzTesting");
 
echo "<pre>";

Staff::setBanned('strackz1');
Staff::setBanned('strackz2');

foreach ($_SESSION['all_staffs'] as $i => $staff) {
    var_dump($staff::getBanned()) . "<br/>";
}


