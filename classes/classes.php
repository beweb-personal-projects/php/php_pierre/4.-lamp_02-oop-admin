<?php

class Player {
    protected $name;
    private $job;
    private $job_rank;
    private const LOGO = "img/logo_player.png";

    function __construct($c_name, $c_job, $c_job_rank) {
        $this->name = $c_name;
        $this->job = $c_job;
        $this->job_rank = $c_job_rank;
    }

    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

    static function getLogo() {
        return self::LOGO;
    }
}

class Staff extends Player{
    private $role;
    private $level;
    private $ignorePermissions;
    private const LOGO = "img/logo_staff.png";
    private static $banned = []; // if i change in one instance, all other instances will have the same data.

    function __construct($c_name, $c_role, $c_level, $c_ignorePermissions) {
        $this->name = $c_name;
        $this->role = $c_role;
        $this->level = $c_level;
        $this->ignorePermissions = $c_ignorePermissions;
    }

    // ----------------------------------------------------------------------------------
    // ALL GETTERS

    function getRole() {
        return $this->role;
    }

    function getLevel() {
        return $this->level;
    }

    function getIgnorePermissions() {
        return $this->ignorePermissions;
    }

    static function getLogo() {
        return self::LOGO;
    }

    static function getBanned() {
        return self::$banned;
    }

    function getDesignation() {
        return "Nickname: {$this->getName()} // Rank: {$this->getRole()}</br>";
    }

    function getStaffArray() {
        return [
            'name' => $this->name,
            'role' => $this->role,
            'level' => $this->level,
            'ignorePermissions' => $this->ignorePermissions,
        ];
    }

    // ----------------------------------------------------------------------------------
    // ALL SETTERS

    function setRole($role) {
        $this->role = $role;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function setIgnorePermissions($ignorePermissions) {
        $this->ignorePermissions = $ignorePermissions;
    }

    static function setBanned($user) {
        array_push(self::$banned, $user);
    }
}